import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import makeRootReducer from './reducers/index';
// import ApiClient from './helpers/ApiClient';
// import clientMidleware from './helpers/clientMidleware';

export default initialState => {
    let middleWare = applyMiddleware( thunk);
    let composeEnhancers = compose;
    const store = createStore(makeRootReducer(), initialState, composeEnhancers(middleWare));
    store.asyncReducers = {};
    store.injectReducer = (key, reducer) => {
        store.asyncReducers[key] = reducer;
        store.replaceReducer(makeRootReducer(store.asyncReducers));
        return store;
    };

    if (module.hot) {
        module.hot.accept('./reducers/index', () => {
            const reducers = require('./reducers/index').default;
            store.replaceReducer(reducers(store.asyncReducers));
        });
    }

    return store;
};
