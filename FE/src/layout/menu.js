import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import menu from '../menu';
import { matchPath, withRouter } from 'react-router';

class AppMenu extends Component {
    isActive = path => {
        return !!matchPath(this.props.location.pathname, path);
    };
    render() {
        return (
            <div className="sidebar">
                <ul className="nav">
                    {menu.map((e, index) => {
                        return (
                            <li key={index} className={`nav-item ${this.isActive(e.path) ? 'active' : ''}`}>
                                <Link className="nav-link " to={e.path}>
                                    <i className={`nav-icon fa ${e.icon}`} />
                                    {e.name}
                                </Link>
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}
export default withRouter(AppMenu);
