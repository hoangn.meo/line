import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import routes from '../routes';
import { ToastContainer } from 'react-toastify';

import AppHeader from './header';
import AppFooter from './footer';
import AppMenu from './menu';
import Breadcrumb from './breadcrumb';
export default class DefaultLayout extends Component {
    render() {
        return (
            <div className="app">
                <ToastContainer toastClassName="toast-container" />
                <AppHeader />
                <div className="app-body">
                    <AppMenu />
                    <div className="main">
                        <Breadcrumb />
                        <Container fluid>
                            <Row>
                                <Col md={12}>
                                    <Switch>
                                        {routes.map((route, idx) => {
                                            return route.component ? (
                                                <Route
                                                    key={idx}
                                                    path={route.path}
                                                    exact={route.exact}
                                                    name={route.name}
                                                    render={props => <route.component {...props} />}
                                                />
                                            ) : null;
                                        })}
                                        <Redirect from="/" to="/" />
                                    </Switch>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </div>
                <AppFooter />
            </div>
        );
    }
}
