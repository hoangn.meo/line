import React, { Component } from 'react';

export default class AppHeader extends Component {
    render() {
        return <header className="app-header">
			<img className='logo' src='https://www.indospgusher.com/images/logo_linetext_icon.png'></img>
		</header>;
    }
}
