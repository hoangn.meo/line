import React, { Component } from 'react';
import { matchPath, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import routes from '../routes';

class Breadcrumb extends Component {
    getActiveRoute = () => {
        return routes.filter(e => !!matchPath(this.props.location.pathname, e.path));
       
    };
    render() {
        let activeRoute = this.getActiveRoute();

        return (
            <nav className="">
                <ol className="breadcrumb">
                    {activeRoute.map((e, index) => {
                        if (index == activeRoute.length - 1) {
                            return (
                                <li key={index} className="active breadcrumb-item" aria-current="page">
                                    {e.name}
                                </li>
                            );
                        }
                        return (
                            <li key={index} className="breadcrumb-item">
                                <Link to={e.path}>{e.name}</Link>
                            </li>
                        );
                    })}                  
                </ol>
            </nav>
        );
    }
}

export default withRouter(Breadcrumb);
