import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class AppFooter extends Component {
    render() {
        return (
            <div className='app-footer'>
                <span>
                    <a href="/" /> &copy; {new Date().getFullYear()} HOANG MEO.
                </span>
                <span className="ml-auto">
                    Powered by <a href="/">HOANG MEO</a>
                </span>
            </div>
        );
    }
}
