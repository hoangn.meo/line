import React from 'react';
import Loadable from 'react-loadable';
import Loading from './components/loading';


const HomePage = Loadable({
    loader: () => import('./views/home'),
    loading: Loading,
});
const TruckInfo = Loadable({
    loader: () => import('./views/truckInfo'),
    loading: Loading,
});
const TruckType = Loadable({
    loader: () => import('./views/truckType'),
    loading: Loading,
});
const CargoType = Loadable({
    loader: () => import('./views/cargoType'),
    loading: Loading,
});
const TruckDriver = Loadable({
    loader: () => import('./views/driver'),
    loading: Loading,
});


const routes = [
	{ path: '/', name: 'Home page', exact:true, component: HomePage },
    { path: '/truck-information', name: 'Truck information', component: TruckInfo },
    { path: '/truck-type', name: 'Truck type', component: TruckType },
    { path: '/cargo-type', name: 'Cargo type', component: CargoType },
    { path: '/truck-driver', name: 'Truck driver', component: TruckDriver },
];

export default routes;
