import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';

class DeleteBtn extends Component {
    constructor(props) {
        super(props);
        this.abort = this.abort.bind(this);
        this.confirm = this.confirm.bind(this);
        this.state = {
            isOpen: true,
        };
    }

    close = () => {
        this.setState({
            isOpen: false,
        });
    };

    abort() {
        this.onAbort();
    }

    confirm() {
        this.onConfirm();
    }

    render() {
        return (
            <Modal isOpen={true} toggle={this.abort} className="modal-danger">
                <ModalHeader toggle={this.abort}>
                    {
                        this.props.title ? this.props.title : 'Confirm delete!'
                    }
                </ModalHeader>
                <ModalBody>{this.props.message}</ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={this.confirm}>
                        {
                            this.props.okLabel ? this.props.okLabel : 'Delete'
                        }
                    </Button>
                    {
                        this.props.showCancelButton ? (
                            <Button color="secondary" onClick={this.abort}>
                                Cancel
                            </Button>
                        ) : null
                    }
                </ModalFooter>
            </Modal>
        );
    }
}

const confirm = (message = 'Are you sure to delete this item?', showCancelButton = true, title = '', okLabel = '') => {
    return new Promise((resolve, reject) => {
        let wrapper = document.body.appendChild(document.createElement('div'));
        let component = ReactDOM.render(<DeleteBtn message={message} showCancelButton={showCancelButton} title={title} okLabel={okLabel}/>, wrapper);
        component.onAbort = () => {
            cleanup();
            reject();
        };
        component.onConfirm = () => {
            cleanup();
            resolve();
        };
        const cleanup = () => {
            component.close();
            ReactDOM.unmountComponentAtNode(wrapper);
            setTimeout(() => {
                wrapper.remove();
            }, 200);
        };
    });
};

export default confirm;
