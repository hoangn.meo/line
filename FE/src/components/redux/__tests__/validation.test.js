import validation from '../validation';

it('TEST VALIDATION: required', () => {
    expect(validation.required('')).not.toBeUndefined();
    expect(validation.required(null)).not.toBeUndefined();
    expect(validation.required('0')).toBeUndefined();
    expect(validation.required('1')).toBeUndefined();
    expect(validation.required('test')).toBeUndefined();
});

it('TEST VALIDATION: truckPlate', () => {
    expect.anything(validation.truckPlate('3B2-22222'));
    expect.anything(validation.truckPlate('23B2-2222'));
    expect.anything(validation.truckPlate('23B2-sasdd'));

    expect(validation.truckPlate('23B2-22222')).toBeUndefined();
    expect(validation.truckPlate('11B2-23534')).toBeUndefined();
    expect(validation.truckPlate('66d2-74545')).toBeUndefined();
});
