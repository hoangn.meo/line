export const required = value => (value != null && `${value}`.length > 0 ? undefined : 'Please fill out this field');

export const integer = value => (value && !/^[0-9]+$/i.test(value) ? 'This value must be integer' : undefined);

export const maxLength = max => value => (value && value.length > max ? `This value must be <=${max} characters` : undefined);
export const minLength = min => value => (value && value.length < min ? `This value must be >=${min} characters` : undefined);

export const numberGt = number => value => (value && Number(value) <= number ? `This value must be >${number}` : undefined);
export const numberGte = number => value => (value && Number(value) < number ? `This value must be >=${number}` : undefined);
export const numberLte = number => value => (value && Number(value) > number ? `This value must be <=${number}` : undefined);
export const truckPlate = value => (value && !/^[0-9]{2}[a-zA-Z]{1}[0-9]{0,1}-[0-9]{5}$/i.test(value) ? 'Invalid truck plate, ex: 20A1-12345' : undefined);

const maxLength500 = maxLength(500);
const maxLength200 = maxLength(200);

const gt0 = numberGt(0);
const gte0 = numberGte(0);
export default {
    required,
    maxLength500,
    gt0,
    gte0,
	integer,
	truckPlate,
	maxLength200
};
