import React, { Component } from 'react';
import { Async } from 'react-select';
import ApiClient from '../../helpers/apiClient';
import { withRouter } from 'react-router-dom';
import utils from '../../utils';
import debounce from 'es6-promise-debounce';

const client = new ApiClient();

class AsyncSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: [],
            url: this.props.url,
        };
        this.loadData = this.loadData.bind(this);
    }

    componentDidMount() {
        let { value, url } = this.props;
        if (value) {
            if (typeof value == 'object') {
                value = value.id;
            }
            url = `${url}${value}/`;
            client.get(url).then(e => {
                if (utils.tryGetData(this, 'refs.thisRef.select.state')) {
                    this.refs.thisRef.select.state.value = e;
                    let { onChange } = this.props;
                    if (onChange) {
                        onChange(e);
                    }
                }
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.url !== this.props.url) {
            this.setState({
                url: nextProps.url,
            });
            this.refs.thisRef.select.state.value = '';
            let { onChange } = this.props;
            if (onChange) {
                onChange();
            }
        }
    }

    loadData(q) {
        let { url } = this.state;
        let { mappingFunc, searchKey } = this.props;
        let __url = `${url}`;
        if (searchKey) {
            __url = url.indexOf('?') == -1 ? `${url}?${searchKey}=${q || ''}` : `${url}&${searchKey}=${q || ''}`;
        }
        return client.get(__url).then(response => {
            if (mappingFunc) {
                return mappingFunc(response);
            }

            return response.data;
        });
    }
    render() {
        let { value } = this.props;
        return (
            <Async key={this.state.url} {...this.props} loadOptions={debounce(this.loadData, 500)} defaultOptions classNamePrefix="core-select" ref="thisRef" />
        );
    }
}

export default withRouter(AsyncSelect);
