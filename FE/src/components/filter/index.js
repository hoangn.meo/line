import React from 'react';
import { FILTER_OP } from '../../constant';
import ListSearch from './list';
export default {
    text: {
        op: FILTER_OP.TEXT,
        cpn: <input className="form-control" />,
        changeFunc: e => e.target.value,
    },
    number: {
        op: FILTER_OP.NUMBER,
        cpn: <input className="form-control" />,
        changeFunc: e => e.target.value,
    },
    list: list => ({
        cpn: <ListSearch list={list} />,
        changeFunc: e => e.value,
    }),
    asyncSelect: Elm => ({
        cpn: Elm,
        changeFuncOnSearch: e => e.id,
    }),
};
