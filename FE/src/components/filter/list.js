import React from 'react';
import Select from 'react-select';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';

const SelectField = ({ value, list, urlParam, location, ...props }) => {
    let urlValue = urlParam ? queryString.parse(location.search)[urlParam] : value;
    let defaultValue, options;
    defaultValue = urlValue ? { value: urlValue, label: list[urlValue] } : '';
    options = list.map((e, i) => ({ value: i + '', label: e }));
    return <Select {...props} defaultValue={defaultValue} options={options} classNamePrefix="core-select" />;
};

export default withRouter(SelectField);
