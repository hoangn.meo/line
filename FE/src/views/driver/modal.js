import React, { Component } from 'react';
import { ModalBody, ModalFooter, Button, Form, FormGroup, Row, Col, Label } from 'reactstrap';
import CModal from '../../components/modal';
import { Field, reduxForm } from 'redux-form';
import CustomField from '../../components/redux/field';
import Validation from '../../components/redux/validation';
import { connect } from 'react-redux';
import ApiClient from '../../helpers/apiClient';
import message from '../../helpers/message';
let client = new ApiClient();

class ObjectModal extends Component {
    submit = form => {
        let { onClose, editingObject } = this.props;
        let api = null;
        if (editingObject) {
            api = client.put(`/truck-driver/${editingObject.id}/`, {
                data: form,
            });
        } else {
            api = client.post(`/truck-driver/`, {
                data: form,
            });
        }
        api.then(() => {
            if (onClose) {
                onClose(true);
            }
            setTimeout(() => {
                let mess = editingObject ? 'Update successful!' : `Create new successful`;
                message.success(mess);
            }, 100);
        }).catch(err => {
            message.error(err.message);
        });
    };

    render() {
        let { editingObject, onClose, handleSubmit } = this.props;
        return (
            <CModal title={editingObject ? `Change truck driver infomation ` : 'Add new truck driver'} onClose={onClose}>
                <div>
                    <Form onSubmit={handleSubmit(form => this.submit(form))} className="form-horizontal">
                        <ModalBody>
                            <Row>
                                <Col md="12">
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label htmlFor="text-input">Fullname</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field component={CustomField} type="text" validate={[Validation.required]} name="fullname" />
                                        </Col>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" type="submit">
                                Submit
                            </Button>
                        </ModalFooter>
                    </Form>
                </div>
            </CModal>
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        initialValues: props.editingObject,
    };
};
ObjectModal = reduxForm({
    form: 'truck-driver-edit-modal',
})(ObjectModal);

export default connect(mapStateToProps)(ObjectModal);
