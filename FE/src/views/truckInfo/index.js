import React, { Component } from 'react';
import { connect } from 'react-redux';
import queryString from 'query-string';
import history from '../../history';
import CoreComponent, { reload } from '../../core';
import utils from '../../utils';
import appFilter from '../../components/filter';
import AsyncSelect from '../../components/filter/asyncSelect';
import EditModal from './modal';
import { TRUCK_STATUS } from '../../constant';

const reducerKey = 'truck-info'; // unique string for each page
const apiUrl = '/truck-info/'; // backend api
const baseUrl = 'truck-information';// frontend url

class Page extends Component {
    state = {
        openModal: false,
        editingObject: null,
        filter: queryString.parse(this.props.location.search),
    };

    onEdit = editingObject => {
        this.setState({
            openModal: true,
            editingObject: editingObject,
        });
    };
    onModalClose = reload => {
        this.setState({
            openModal: false,
        });
        reload && this.reload();
    };
    reload = () => {
        this.props.dispatch(reload(reducerKey));
    };
    
    renderTableBody = z => {
        let cargoText = z.cargo_types.map(e => e.cargo.name).join(', ');
        return (
            <React.Fragment>
                <td>{z.plate}</td>
                <td className="auto_trim_text" style={{ maxWidth: 250 }} title={cargoText}>
                    {cargoText}
                </td>
                <td>{z.driver.fullname}</td>
                <td>{z.truck_type.name}</td>
                <td>{utils.formatPrice(z.price)}</td>
                <td>{z.dimension}</td>
                <td className="auto_trim_text" style={{ maxWidth: 200 }} title={z.parking_address}>
                    {z.parking_address}
                </td>
                <td>{z.year}</td>
                <td>{TRUCK_STATUS[z.status]}</td>
                <td className="auto_trim_text" style={{ maxWidth: 200 }} title={z.description}>
                    {z.description}
                </td>
            </React.Fragment>
        );
    };
    render() {
        let { openModal, editingObject } = this.state;

        let tableHeader = [
            {
                header: 'Plate',
                accessor: 'plate',
                filter: appFilter.text,
            },
            {
                header: 'Cargo Type',
                unOrder: true,
            },
            {
                header: 'Driver',
                accessor: 'driver_id',

                filter: appFilter.asyncSelect(
                    <AsyncSelect getOptionLabel={({ fullname }) => fullname} getOptionValue={({ id }) => id} name="fullname" url="/truck-driver/" />,
                ),
            },
            {
                header: 'Type',
                accessor: 'truck_type_id',
                filter: appFilter.asyncSelect(
                    <AsyncSelect getOptionLabel={({ name }) => name} getOptionValue={({ id }) => id} name="name" url="/truck-type/" />,
                ),
            },
            {
                header: 'Price',
                accessor: 'price',
                filter: appFilter.number,
            },
            {
                header: 'Dimension',
                accessor: 'dimension',
                filter: appFilter.text,
            },
            {
                header: 'Parking address',
                accessor: 'parking_address',
                filter: appFilter.text,
            },
            {
                header: 'Production Year',
                accessor: 'year',
                filter: appFilter.number,
            },
            {
                header: 'Status',
                accessor: 'status',
                filter: appFilter.list(TRUCK_STATUS),
            },
            {
                header: 'Description',
                accessor: 'description',
                filter: appFilter.text,
            },
        ];
        return (
            <React.Fragment>
                <CoreComponent
                    reducerKey={reducerKey}
                    baseUrl={baseUrl}
                    apiUrl={apiUrl}
                    tableHeader={tableHeader}
                    renderBody={this.renderTableBody}
                    reload={this.reload}
                    onEdit={this.onEdit}
                    onAddNew={this.onEdit}
                />

                {openModal && <EditModal onClose={this.onModalClose} editingObject={editingObject} />}
            </React.Fragment>
        );
    }
}

export default connect()(Page);
