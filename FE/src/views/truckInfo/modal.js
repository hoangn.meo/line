import React, { Component } from 'react';
import { ModalBody, ModalFooter, Button, Form, FormGroup, Row, Col, Label } from 'reactstrap';
import CModal from '../../components/modal';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import CustomField from '../../components/redux/field';
import AsyncSelect from '../../components/redux/asyncSelect';
import PriceField from '../../components/redux/price';

import Validation from '../../components/redux/validation';
import { connect } from 'react-redux';
import ApiClient from '../../helpers/apiClient';
import message from '../../helpers/message';
import { TRUCK_STATUS } from '../../constant';
let client = new ApiClient();

class ObjectModal extends Component {
    submit = form => {
        let { onClose, editingObject } = this.props;
        let data = {
            ...form,
            cargo_types: null,
            driver: null,
            truck_type: null,
            truck_type_id: form.truck_type.id,
            driver_id: form.driver.id,
            cargo_types: form.cargo_types.map(e => e.id),
        };
        let api = null;
        if (editingObject) {
            api = client.put(`/truck-info/${editingObject.id}/`, {
                data: data,
            });
        } else {
            api = client.post(`/truck-info/`, {
                data: data,
            });
        }
        api.then(() => {
            if (onClose) {
                onClose(true);
            }
            setTimeout(() => {
                let mess = editingObject ? 'Update successful!' : `Create new successful`;
                message.success(mess);
            }, 100);
        }).catch(err => {
            message.error(err.message);
        });
    };

    render() {
        let { editingObject, onClose, handleSubmit } = this.props;
        return (
            <CModal title={editingObject ? `Change truck infomation` : 'Add new truck'} onClose={onClose} className="modal-lg">
                <div>
                    <Form onSubmit={handleSubmit(form => this.submit(form))} className="form-horizontal">
                        <ModalBody>
                            <Row>
                                <Col md="6">
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label htmlFor="text-input">
                                                Truck plate<span className="input-required">*</span>
                                            </Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field component={CustomField} type="text" validate={[Validation.required, Validation.truckPlate]} name="plate" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label htmlFor="text-input">
                                                Price<span className="input-required">*</span>
                                            </Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field component={PriceField} type="text" validate={[Validation.required, Validation.gte0]} name="price" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label htmlFor="text-input">
                                                Status<span className="input-required">*</span>
                                            </Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field component={CustomField} type="select" validate={[Validation.required]} name="status">
                                                {TRUCK_STATUS.map((e, index) => {
                                                    return (
                                                        <option key={index} value={index}>
                                                            {e}
                                                        </option>
                                                    );
                                                })}
                                            </Field>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label htmlFor="text-input">Dimemsion</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field component={CustomField} type="text" name="dimension" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label htmlFor="text-input">Parking Address</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field
                                                component={CustomField}
                                                type="textarea"
                                                validate={[Validation.maxLength500]}
                                                displayCounter
                                                maxLength={500}
                                                name="parking_address"
                                            />
                                        </Col>
                                    </FormGroup>
                                </Col>
                                <Col md="6">
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label>
                                                Driver<span className="input-required">*</span>
                                            </Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field
                                                component={AsyncSelect}
                                                validate={[Validation.required]}
                                                getOptionLabel={({ fullname }) => fullname}
                                                getOptionValue={({ id }) => id}
                                                name="driver"
                                                url="/truck-driver/"
                                                mappingFunc={res => res.data}
                                                searchKey="fullname"
                                            />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label>
                                                Cargo type<span className="input-required">*</span>
                                            </Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field
                                                component={AsyncSelect}
                                                validate={[Validation.required]}
                                                getOptionLabel={({ name }) => name}
                                                getOptionValue={({ id }) => id}
                                                name="cargo_types"
                                                url="/cargo-type/"
                                                mappingFunc={res => res.data}
                                                searchKey="name"
                                                isMulti={true}
                                            />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label>
                                                Truck type<span className="input-required">*</span>
                                            </Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field
                                                component={AsyncSelect}
                                                validate={[Validation.required]}
                                                getOptionLabel={({ name }) => name}
                                                getOptionValue={({ id }) => id}
                                                name="truck_type"
                                                url="/truck-type/"
                                                mappingFunc={res => res.data}
                                                searchKey="name"
                                            />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label htmlFor="text-input">Production Year</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field component={CustomField} type="text" validate={[Validation.integer]} name="year" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="3">
                                            <Label htmlFor="text-input">Description</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Field
                                                component={CustomField}
                                                validate={[Validation.maxLength200]}
                                                displayCounter
                                                maxLength={200}
                                                type="textarea"
                                                name="description"
                                            />
                                        </Col>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" type="submit">
                                Submit
                            </Button>
                        </ModalFooter>
                    </Form>
                </div>
            </CModal>
        );
    }
}
const FORM_NAME = 'truck-info-edit-modal';
const MAX_CARGO_ITEMS = 10;
const mapStateToProps = (state, props) => {
    let formSelector = formValueSelector(FORM_NAME);
    let { editingObject } = props;
    return {
        initialValues: {
            ...editingObject,
            status: editingObject ? editingObject.status : 0,
            cargo_types: editingObject ? editingObject.cargo_types.map(e => e.cargo) : [],
        },
    };
};
ObjectModal = reduxForm({
    form: FORM_NAME,
    validate: form => {
        let error = {};
        let cargo_types = form.cargo_types || [];
        if (cargo_types.length > MAX_CARGO_ITEMS) {
            error['cargo_types'] = `Please select less than ${MAX_CARGO_ITEMS} items`;
        }
        return error;
    },
})(ObjectModal);

export default connect(mapStateToProps)(ObjectModal);
