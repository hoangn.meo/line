import React, { Component } from 'react';
import { connect } from 'react-redux';
import CoreComponent, { reload } from '../../core';
import appFilter from '../../components/filter';
import EditModal from './modal';

const reducerKey = 'truck-type';
const apiUrl = '/truck-type/';
const baseUrl = 'truck-type';

class Page extends Component {
    state = {
        openModal: false,
        editingObject: null,
    };
    onEdit = editingObject => {
        this.setState({
            openModal: true,
            editingObject: editingObject,
        });
    };
    onModalClose = reload => {
        this.setState({
            openModal: false,
        });
        reload && this.reload();
    };
    reload = () => {
        this.props.dispatch(reload(reducerKey));
    };
    renderTableBody = z => {
        return (
            <React.Fragment>
                <td>{z.id}</td>
                <td>{z.name}</td>
            </React.Fragment>
        );
    };
    render() {
        let tableHeader = [
            { header: 'Id', accessor: 'id', width: 200 },
            {
                header: 'Truck type',
                accessor: 'name',
                filter: appFilter.text,
            },
        ];
        let { openModal, editingObject } = this.state;
        return (
            <React.Fragment>
                <CoreComponent
                    reducerKey={reducerKey}
                    baseUrl={baseUrl}
                    apiUrl={apiUrl}
                    tableHeader={tableHeader}
                    renderBody={this.renderTableBody}
                    reload={this.reload}
                    onEdit={this.onEdit}
                    onAddNew={this.onEdit}
                />
                {openModal && <EditModal onClose={this.onModalClose} editingObject={editingObject} />}
            </React.Fragment>
        );
    }
}

export default connect()(Page);
