export default [
	{path:"/truck-information",icon:'fa-truck', name:"Truck information"}, 
	{path:"/truck-type",icon:'fa-file-text-o', name:"Truck type"}, 
	{path:"/cargo-type",icon:'fa-database', name:"Cargo type"}, 
	{path:"/truck-driver",icon:'fa-user-o', name:"Truck driver"}, 
]