import React from 'react';
import { Button } from 'reactstrap';

const ActionStyle = {
    height: 25,
    marginTop: 3,
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 0,
};
export const EditBtn = props => {
    return (
        <Button size="sm" color="warning keep-selected" {...props} style={ActionStyle}>
            <i className="fa fa-pencil keep-selected" />
            &nbsp; Edit
        </Button>
    );
};
export const DeleteBtn = props => {
    return (
        <Button size="sm" color="danger keep-selected" {...props} style={ActionStyle}>
            <i className="fa fa-trash-o keep-selected" />
            &nbsp; Delete
        </Button>
    );
};

export const AddBtn = props => {
    return (
        <Button size="sm" color="success" {...props} style={{ ...ActionStyle, float: 'right' }}>
            <i className="fa fa-plus" />
            &nbsp; Add
        </Button>
    );
};
export const ClearFilterBtn = props => {
    return (
        <Button size="sm" color="info" {...props} style={{ ...ActionStyle }}>
            <i className="fa fa-close" />
            &nbsp; Clear filter
        </Button>
    );
};
