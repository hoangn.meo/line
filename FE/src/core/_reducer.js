import { ON_LOADING, ON_GET_LIST, ON_LOAD_ERROR, ON_RELOAD } from './_action';

const createReducer = key => {
    return (state = { loading: false }, { type, payload }) => {
        switch (type) {
            case ON_LOADING(key):
                return {
                    ...state,
                    loading: true,
                    type,
                };
            case ON_GET_LIST(key):
                return {
                    ...state,
                    data: payload.data,
                    pageIndex: payload.pageIndex,
                    pageSize: payload.pageSize,
                    total: payload.total,
                    loading: false,
                    type,
                };
            case ON_LOAD_ERROR(key):
                return {
                    ...state,
                    loading: false,
                    type,
                };
            case ON_RELOAD(key):
                return {
                    ...state,
                    loading: false,
					type,
					resetTime:new Date().getTime()
                };
            default:
                return state
        }
    };
};
export default createReducer;
