import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';
import { Popover, PopoverHeader, PopoverBody, Button } from 'reactstrap';
import { FILTER_TEXT_MAPPER } from '../constant';
let count = 0;

class RowFilter extends Component {
    guid = 'tooltip-' + count++;
    constructor(props) {
        super(props);
        this.state = {
            ...this.getFilter(),
            popoverOpen: false,
        };
    }

    getFilter = () => {
        let { location, row } = this.props;
        let params = queryString.parse(location.search);
        if (params[row.accessor]) {
            let p = params[row.accessor].split('.');
            if (p.length == 1) {
                return {
                    op: 'eq',
                    value: params[row.accessor],
                };
            } else if (p.length == 2) {
                return {
                    op: p[0],
                    value: p[1],
                };
            }
        }
        return {
            op: '',
            value: '',
        };
    };
    toggle = () => {
        this.setState({
            popoverOpen: !this.state.popoverOpen,
        });
    };
    onFilterValueChange = e => {
        let { filter } = this.props.row;
        let value = e;
        if (filter.changeFunc) {
            value = filter.changeFunc(value);
        }
        this.setState({
            value: value,
        });
    };
    search = () => {
        let { op, value } = this.state;
        let { accessor, filter } = this.props.row;
        if (filter.changeFuncOnSearch) {
            value = filter.changeFuncOnSearch(value);
        }
        op = op || 'eq';
        this.props.onSearchChange(accessor, `${op}.${value}`);
        this.toggle();
    };
    clear = () => {
        let { row } = this.props;
        let { accessor } = row;
        this.props.onSearchChange(accessor, null, true);
        this.toggle();
    };
    render() {
        let { op } = this.state;
        let { location, row } = this.props;
        let { header, filter } = row;
        let params = queryString.parse(location.search);
        let active = !!params[row.accessor];
        return (
            <div className={`tb-filter ${active ? '' : 'in-active'}`}>
                <i className="fa fa-filter" id={this.guid} onClick={this.toggle} />
                <Popover isOpen={this.state.popoverOpen} target={this.guid}>
                    <PopoverHeader>
                        Filter by: {header}
                        <button onClick={this.toggle} className="btn btn-link x3" style={{ position: 'absolute', top: -2, right: 0, color: 'red' }}>
                            <i className="fa fa-close" />
                        </button>
                    </PopoverHeader>
                    <PopoverBody>
                        <div className="row">
                            {filter.op && (
                                <div className="col-md-12" style={{ marginBottom: 5 }}>
                                    <select className="form-control" value={op} onChange={e => this.setState({ op: e.target.value })}>
                                        {filter.op.map(e => (
                                            <option key={e} value={e}>
                                                {FILTER_TEXT_MAPPER[e]}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                            )}
                            <div className="col-md-12">
                                {React.cloneElement(filter.cpn, {
                                    onChange: this.onFilterValueChange,
                                    value: this.state.value,
                                })}
                            </div>
                            <div className="col-md-6" style={{ marginTop: 5 }}>
                                <Button onClick={this.search} className="btn-block btn-square" color="primary">
                                    <i className="fa fa-search" />
                                    <span>&nbsp;Search</span>
                                </Button>
                            </div>
                            <div className="col-md-6" style={{ marginTop: 5 }}>
                                <Button onClick={this.clear} className="btn-block btn-square" color="danger">
                                    <i className="fa fa-close" />
                                    <span>&nbsp;Clear</span>
                                </Button>
                            </div>
                        </div>
                    </PopoverBody>
                </Popover>
            </div>
        );
    }
}

export default withRouter(RowFilter);
