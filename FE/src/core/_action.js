export const ON_LOADING = key => `APP_BASE_ACTION_ON_LOADING:${key}`;
export const ON_GET_LIST = key => `APP_BASE_ACTION_ON_GET_LIST:${key}`;
export const ON_LOAD_ERROR = key => `APP_BASE_ACTION_ON_LOAD_ERROR:${key}`;
export const ON_RELOAD = key => `APP_BASE_ACTION_ON_RELOAD:${key}`;
export const ON_LOAD_COMPLETED = key => `APP_BASE_ACTION_ON_LOAD_COMPLETED:${key}`;