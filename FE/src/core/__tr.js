import React, { PureComponent } from 'react';

export default class BaseTr extends PureComponent {
    render() {
        return (
            <tr className="hover" {...this.props}>
                {this.props.children}
            </tr>
        );
    }
}
