import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, Col, Row, Table } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { onLoadCompleted } from './index';
import confirmDelete from '../components/confirmDelete';
import Pagination from '../components/pagination';
import history from '../history';
import queryString from 'query-string';
import Loading from '../components/loading';
import { ON_RELOAD } from './_action';
import ApiClient from '../helpers/apiClient';
import createReducer from './_reducer';
import message from '../helpers/message';
import BaseTr from './__tr';
import RowFilter from './_rowfilter';
import { AddBtn, ClearFilterBtn, DeleteBtn, EditBtn } from './_actionBtn';

const client = new ApiClient();
const DEFAULT_ORDER = '-id';

const getOrder = (order = DEFAULT_ORDER) => {
    if (order.startsWith('-')) {
        return { order: order.substring(1), desc: true };
    }
    return { order: order, desc: false };
};
const createOrder = (key, desc) => {
    return (desc ? `-` : '') + key;
};
const getTrColor = (selectedRecord, z) => {
    return selectedRecord == z ? 'rgb(154, 255, 193)' : null;
};

class CoreComponent extends Component {
    static contextTypes = {
        store: PropTypes.object.isRequired,
    };
    state = {
        __data: {},
        loading: false,
        selectedRecord: null,
    };

    resetTime = new Date().getTime();
    unsubscribe = null;

    componentDidMount() {
        let { reducerKey } = this.props;
        let reducer = createReducer(reducerKey);
        this.context.store.injectReducer(reducerKey, reducer);
        let searchKey = this.props.location.search;
        this.loadData(searchKey);
        this.unsubscribe = this.context.store.subscribe(() => {
            let reducer = this.context.store.getState()[reducerKey];
            if (reducer.type === ON_RELOAD(reducerKey)) {
                if (this.resetTime != reducer.resetTime) {
                    this.loadData(this.props.location.search);
                }
                this.resetTime = reducer.resetTime;
            }
        });
    }
    getData = (query, key, apiUrl) => {
        let { mappingFunc, checkBeforGetData } = this.props;
        if (checkBeforGetData) {
            if (!checkBeforGetData()) {
                return;
            }
        }
        this.onSelectedRecordChange();
        this.setState({
            loading: true,
        });
        window.setTimeout(() => {
            client
                .get(`${apiUrl}`, {
                    params: query,
                })
                .then(data => {
                    if (mappingFunc) {
                        data = mappingFunc(data);
                    }
                    if (data && data.data && data.data.length == 0 && data.page > 1) {
                        let { page } = data;
                        this.onPageChange(page - 1);
                        return;
                    }
                    this.setState({
                        __data: data,
                        loading: false,
                    });
                    this.context.store.dispatch(onLoadCompleted(key));
                })
                .catch(() => {
                    this.setState({
                        __data: {},
                        loading: false,
                    });
                });
        });
    };
    componentWillUnmount() {
        if (this.unsubscribe) {
            this.unsubscribe();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.location !== this.props.location) {
            this.loadData(nextProps.location.search);
        }
    }

    onPageChange = p => {
        let { baseUrl, location } = this.props;
        let params = queryString.parse(location.search);
        params.page = p;
        let url = queryString.stringify(params);
        history.push(`/${baseUrl}?${url}`);
    };
    loadData = search => {
        let { reducerKey, apiUrl, order, noOrder } = this.props;
        let params = queryString.parse(search);
        if (noOrder) {
            params['order'] = null;
        } else {
            params['order'] = params['order'] || order || DEFAULT_ORDER;
        }

        this.getData(params, reducerKey, apiUrl);
    };

    onDelete = () => {
        let { selectedRecord } = this.state;
        let { apiUrl } = this.props;
        if (selectedRecord) {
            confirmDelete().then(() => {
                client
                    .del(`${apiUrl}${selectedRecord.id}/`)
                    .then(() => {
                        message.success('Delete successful!');
                        this.loadData(this.props.location.search);
                    })
                    .catch(() => {
                        message.error(`Delete failed!`);
                    });
            });
        }
    };
    onEdit = () => {
        let { selectedRecord } = this.state;
        if (selectedRecord) {
            this.props.onEdit(selectedRecord);
        }
    };

    onAddNew = () => {
        this.props.onAddNew();
    };
    onSelectedRecordChange = record => {
        if (this.props.onRowClick) {
            this.props.onRowClick(record);
        }
        this.setState({
            selectedRecord: record,
        });
    };
    onOrderChange = e => {
        if (e.unOrder) {
            return;
        }
        let { baseUrl, location } = this.props;
        let params = queryString.parse(location.search);
        const pageOrder = getOrder(params.order);
        if (pageOrder.order == e.accessor) {
            params.order = createOrder(e.accessor, !pageOrder.desc);
        } else {
            params.order = e.accessor;
        }
        let url = queryString.stringify(params);
        history.push(`/${baseUrl}?${url}`);
    };
    onSearchChange = (k, v, clear) => {
        let { baseUrl, location } = this.props;
        let params = queryString.parse(location.search);
        if (clear) {
            delete params[k];
        } else {
            params[k] = v;
        }

        let url = queryString.stringify(params);
        history.push(`/${baseUrl}?${url}`);
    };
    clearFilter = () => {
        let { baseUrl } = this.props;
        history.push(`/${baseUrl}`);
    };
    render() {
        let { __data, loading, selectedRecord } = this.state;
        let { tableHeader, renderBody, minWidth,  maxHeight, location } = this.props;
        let { onEdit, onAddNew, hideDeleteBtn } = this.props;
        let { page, records, total, data } = __data;
        let defaultHeight = window.innerHeight - 300;
        defaultHeight = defaultHeight < 400 ? 400 : defaultHeight;
        const pageOrder = getOrder(queryString.parse(location.search).order);
        return (
            <div className="animated fadeIn">
                {loading && <Loading />}
                <Row>
                    <Col xs="12" lg="12">
                        <Card>
                            <CardBody>
                                <div className="table-action-btn">
                                    {onEdit && <EditBtn disabled={!selectedRecord} onClick={this.onEdit} />}
                                    {!hideDeleteBtn && <DeleteBtn disabled={!selectedRecord} onClick={this.onDelete} />}
                                    {onAddNew && <AddBtn onClick={this.onAddNew} />}
                                    <ClearFilterBtn onClick={this.clearFilter} />
                                </div>
                                <div className="table-main-region">
                                    <div style={{ minWidth: minWidth, maxHeight: maxHeight || defaultHeight }}>
                                        <Table className={`app-table tbl-oneline fix-header`} bordered>
                                            <thead>
                                                <tr>
                                                    {tableHeader.map((e, index) => {
                                                        let _class = '',
                                                            _style = { width: e.width };
                                                        if (!e.unOrder) {
                                                            _class += 'tb-order';
                                                        }
                                                        return (
                                                            <th key={index} className={_class} style={_style}>
                                                                {e.header}
                                                                {!e.unOrder && pageOrder.order == e.accessor && (
                                                                    <i
                                                                        onClick={() => this.onOrderChange(e)}
                                                                        className={`fa fa-long-arrow-up ${pageOrder.desc ? 'desc' : ''}`}
                                                                    />
                                                                )}
                                                                {!e.unOrder && pageOrder.order != e.accessor && (
                                                                    <i onClick={() => this.onOrderChange(e)} className={`fa fa-long-arrow-up in-active`} />
                                                                )}
                                                                {e.filter && <RowFilter row={e} onSearchChange={this.onSearchChange} />}
                                                            </th>
                                                        );
                                                    })}
                                                </tr>
                                            </thead>

                                            <tbody>
                                                {(!data || data.length == 0) && (
                                                    <tr>
                                                        <td colSpan={tableHeader.length} className="text-center">
                                                            KHÔNG CÓ KẾT QUẢ TÌM KIẾM
                                                        </td>
                                                    </tr>
                                                )}

                                                {data &&
                                                    data.map((z, index) => {
                                                        return (
                                                            <BaseTr
                                                                onClick={() => this.onSelectedRecordChange(z)}
                                                                style={{ backgroundColor: getTrColor(selectedRecord, z) }}
                                                                key={index}
                                                            >
                                                                {renderBody(z)}
                                                            </BaseTr>
                                                        );
                                                    })}
                                            </tbody>
                                        </Table>
                                    </div>
                                </div>
                                {records && page && <Pagination pageSize={records} total={total} page={page} onPageChange={this.onPageChange} />}
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

CoreComponent = withRouter(CoreComponent);

export default CoreComponent;
