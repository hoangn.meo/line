import CoreComponent from './_component';
import { ON_RELOAD, ON_LOAD_COMPLETED } from './_action';

export const reload = key => {
    return {
        type: ON_RELOAD(key),
    };
};

export const onLoadCompleted = key => {
    return {
        type: ON_LOAD_COMPLETED(key),
    };
};

export default CoreComponent;
