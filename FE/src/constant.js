export const FILTER_OP = {
    TEXT: ['eq', 'like', 'stw', 'endw'],
    DATE: ['gt', 'gte', 'lt', 'lte'],
    NUMBER: ['eq', 'gte', 'gt', 'lte', 'lt'],
};

export const TRUCK_STATUS = ['In-use', 'New', 'Stopped'];

export const FILTER_TEXT_MAPPER = {
    eq: 'Equal',
    like: 'Contains',
    gt: 'Greater than',
    gte: 'Greater than or equal to',
    lt: 'Less than',
    lte: 'Less than or equal to',
    stw: 'Start with',
    endw: 'End with',
};
