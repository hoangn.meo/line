import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
const makeRootReducer = asyncReducers => {
  return combineReducers({
    form: formReducer,
    ...asyncReducers
  });
};

export default makeRootReducer;
