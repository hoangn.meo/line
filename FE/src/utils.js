const tryGetData = (obj, key, defaultValue = '') => {
    try {
        let d = obj;
        let arr = key.split('.');
        arr.forEach(e => {
            d = d[e];
        });
        return d;
    } catch (error) {
        return defaultValue;
    }
};
const formatPrice = (price) => {
    return price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
};

export default {
	tryGetData,
	formatPrice
}
