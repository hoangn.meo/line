import superagent from 'superagent';
import config from '../config';
const methods = ['get', 'post', 'put', 'path', 'del'];

export const API_HOST = config.API;
let formatUrl = path => {
    if (path.startsWith('http')) return path;
    return API_HOST + path;
};
export default class ApiClient {
    constructor() {
        methods.forEach(method => {
            this[method] = (path, { params, files, data } = {}) => {
                return new Promise((resolve, reject) => {
                    const request = superagent[method](formatUrl(path));
                    request.set('Authorization', "Bearer 4ec5e4f4-9d87-4c1c-9e11-f7ab187896f0");
                    if (params) {
                        request.query(params);
                    }
                    if (data) {
                        request.send(data);
                    }
                    request.end((error, { body } = {}) => {
                        if (error) {
                            reject(body || error);
                        } else {
                            resolve(body);
                        }
                    });
                });
            };
        });
    }
    empty() {}
}
