import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { HashRouter, Route, Router, Switch } from 'react-router-dom';
import './app.scss';
import { Provider } from 'react-redux';
import history from './history';

import Layout from './layout';

class App extends Component {
    static childContextTypes = {
        store: PropTypes.object,
    };
    getChildContext() {
        return {
            store: this.props.store,
        };
    }
    render() {
        return (
            <Provider store={this.props.store}>
                <Router history={history}>
                    <Switch>
                        <Route path="/" name="Home" component={Layout} />
                    </Switch>
                </Router>
            </Provider>
        );
    }
}

export default App;
