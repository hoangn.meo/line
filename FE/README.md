This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts
	- `npm start` 		|-> Runs the app in the development mode
	- `npm test` 		|-> Launches the test runner in the interactive watch mode
	- `npm run build` 	|-> Builds the app for production to the `build` folder
	- `npm run deploy`	|-> Builds and deploy to firebase hosting (Xin xem thêm hướng dẫn của google firebase hosting)

### Cấu hình
	- Thay đổi Backend API trong file .env.local (dev mode) và .env.production (production mode)

### Yêu cầu API backend
	- Các table phải có tên khóa chính là "id"
	- Hỗ trợ query theo các field trực tiếp trên url params, 
	- Hỗ trợ các toán tử  =, >, >=, <, <=, start_with, end_with, contains
	- Hỗ trợ order , order={field_name} (ASC), order={-field_name} (DASC)
### Hướng dẫn sử dụng
	- Tạo page mới trong /src/views
	- Register routes -> /src/routes.js
	- Thêm vào menu -> src/menu.js
	- Trong từng page chỉ cần thay đổi:
		- reducerKey 		: Unique string
		- apiUrl  	 		: Backend url
		- baseUrl  	 		: uUrl trên frontend
		- onEdit	 		: Action khi user click edit button
		- onModalClose  	: Action khi modal create, update close
		- reload			: Reload page action
		- renderTableBody	: Render table body
		- tableHeader
			- header 	: header name,
			- accessor	: tên field trong database,
			- filter	: loại filter
### Thành quả
	- Do việc sử dụng [Create React App] do facebook cung cấp nên
		- Sử dụng đơn giản
		- Auto reload,
		- Tối ưu production mode
		- Multi environment
		- ...
	- Code Splitting
	- Không phải code quá nhiều khi tạo màn hình quản lý mới
		- Tự phân trang
		- Filter, order theo từng field
		- Nhiều kiểu filter , text, number ...		
	- Form create và update data được dùng chung
	- ...
