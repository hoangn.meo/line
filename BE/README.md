## Yêu cầu hệ thống
	- Python >=3.5
	- Pip
	- Mysql or Mariadb
## Cấu hình
	- Thay đổi cấu hình database trong file core/setting.py
		DATABASES = {
			"default": {
				"ENGINE": "django.db.backends.mysql",
				"NAME": os.environ.get("DB_NAME", "db_name"),
				"USER": os.environ.get("DB_USER", "db_user"),
				"PASSWORD": os.environ.get("DB_PASS", "db_pass"),
				"HOST": os.environ.get("DB_HOST", "db_host"),
				"PORT": "db_port",
				"CONN_MAX_AGE": 600,
			}
		}
 
	
## Hướng dẫn cài đặt
	- Thực hiện tuần tự các câu lệnh:
		pip install -r requirements.txt
		python manage.py makemigrations
		python manage.py migrate
		python manage.py runserver
	- Để deploy product environment có thể sử dụng gunicorn,nginx
		Chi tiết tại https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-18-04
		
## API
	- Truck Driver:
		GET:  	http://localhost:8000/api/v1/truck-driver/  (get list)
		GET:	http://localhost:8000/api/v1/truck-driver/<id>/ (get one with id)
		POST: 	http://localhost:8000/api/v1/truck-driver/ (create truck driver)
		PUT:  	http://localhost:8000/api/v1/truck-driver/<id>/ (update truck driver)
		DELETE: http://localhost:8000/api/v1/truck-driver/<id>/ (delete truck driver)
	- Truck Type:
		GET:  	http://localhost:8000/api/v1/truck-type/  (get list)
		GET:	http://localhost:8000/api/v1/truck-type/<id>/ (get one with id)
		POST: 	http://localhost:8000/api/v1/truck-type/ (create truck type)
		PUT:  	http://localhost:8000/api/v1/truck-type/<id>/ (update truck type)
		DELETE: http://localhost:8000/api/v1/truck-type/<id>/ (delete truck type)
	- Cargo type:
		GET:  	http://localhost:8000/api/v1/cargo-type/  (get list)
		GET:	http://localhost:8000/api/v1/cargo-type/<id>/ (get one with id)
		POST: 	http://localhost:8000/api/v1/cargo-type/ (create cargo type)
		PUT:  	http://localhost:8000/api/v1/cargo-type/<id>/ (update cargo type)
		DELETE: http://localhost:8000/api/v1/cargo-type/<id>/ (delete cargo type)
	- Truck Infomation:
		GET:  	http://localhost:8000/api/v1/truck-info/  (get list)
		GET:	http://localhost:8000/api/v1/truck-info/<id>/ (get one with id)
		POST: 	http://localhost:8000/api/v1/truck-info/ (create truck info)
		PUT:  	http://localhost:8000/api/v1/truck-info/<id>/ (update truck info)
		DELETE: http://localhost:8000/api/v1/truck-info/<id>/ (delete truck info)