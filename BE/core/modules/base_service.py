import math


class Paginator(object):
	showBegin = False

	def __init__(self, page, records, total):
		self.page = page
		self.records = records
		self.total = total

	@property
	def totalPages(self):
		return math.ceil(self.total / self.records)
	@property
	def arrayPages(self):
		return range(1, self.totalPages + 1)



class BaseService(object):
	__page = 1
	__records = 10
	__orderBy = "-id"
	__defaultFilter = []

	def __init__(self, model, **kwargs):
		self.model = model
		if "page" in kwargs:
			self.__page = int(kwargs["page"])
		if "records" in kwargs:
			self.__records = int(kwargs["records"])
		if "orderBy" in kwargs:
			self.__orderBy = kwargs["orderBy"]
		if "defaultFilter" in kwargs:
			self.__defaultFilter = kwargs["defaultFilter"]

	def getById(self, _id):
		try:
			return self.model.objects.get(pk=_id)
		except self.model.DoesNotExist:
			return None

	def count(self, _filter):
		return self.__find(_filter).count()

	def findOne(self, _filter=None, _order=None):
		querySet = self.__find(_filter)
		return self.__order(querySet, _order).first()

	def findAll(self):
		return self.model.objects.all()

	def findAllWithFilterOrder(self, _filter=None, _order=None):
		querySet = self.__find(self.__defaultFilter)
		querySet = self.__find(_filter, querySet)
		querySet = self.__order(querySet, _order)
		return querySet.all()

	def find(self, _filter=None, _order=None, **kwargs):
		currentPage = int(kwargs.get("page", self.__page))
		records = int(kwargs.get("records", self.__records))
		excludeGetTotal = "excludeGetTotal" in kwargs
		start = (currentPage - 1) * records
		querySet = self.__find(self.__defaultFilter)
		querySet = self.__find(_filter, querySet)
		querySet = self.__order(querySet, _order).all()
		if excludeGetTotal:
			return querySet[start : start + records]

		count = querySet.count()
		items = querySet[start : start + records]
		if "select" in kwargs and kwargs['select'] != None:
			_select = kwargs['select']
			items = items.values(*_select)
		paginator = Paginator(currentPage, records, count)
		return (items, paginator)

	def __find(self, _filter, querySet=None):
		querySet = self.model.objects if querySet is None else querySet
		if _filter is None:
			return querySet
		elif isinstance(_filter, (list, tuple)):
			for f in _filter:
				querySet = querySet.filter(f)
			return querySet
		else:
			return querySet.filter(_filter)

	def __order(self, querySet, _order):
		if _order is None:
			querySet = querySet.order_by(self.__orderBy)
		elif isinstance(_order, (list, tuple)):
			querySet = querySet.order_by(*_order)
		else:
			querySet = querySet.order_by(_order)

		return querySet
