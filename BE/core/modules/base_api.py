import json
from django.http import JsonResponse, HttpResponse
from django.views.generic import View
from django.forms.models import model_to_dict
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import serializers


def dump(obj, isModel):
    return model_to_dict(obj) if isModel else dict(obj)


def toJson(res, **header):
    res = JsonResponse(res, safe=False)
    for (key, val) in header.items():
        res.setdefault(key, val)
    return res


def createSerializers(model):
    class MyModelSerializer(serializers.ModelSerializer):
        class Meta:
            model = model
            fields = "__all__"
    return MyModelSerializer


_OPERATORS = {
    'like': lambda field, val: {"{}__icontains".format(field): val},
    'gt': lambda field, val: {"{}__gt".format(field): val},
    'gte': lambda field, val: {"{}__gte".format(field): val},
    'lt': lambda field, val: {"{}__lt".format(field): val},
    'lte': lambda field, val: {"{}__lte".format(field): val},
    'eq': lambda field, val: {"{}".format(field): val},
    'stw': lambda field, val: {"{}__startswith".format(field): val},
    'endw': lambda field, val: {"{}__endswith".format(field): val},
}


class BaseAPI(APIView):

    def __init__(self, model, service, serializers=None, **kwargs):
        self.model = model
        self.service = service
        self.serializers = serializers

    def __get_item(self, request, id):
        item = self.service.getById(id)
        if item is None:
            return HttpResponse(status=404)
        return Response(model_to_dict(item))

    def get(self, request, id=None, *args, **kwargs):
        if id:
            return self.__get_item(request, id)
        _filter = []
        _order = None
        _include = None
        _include_set = None
        _select = None
        queryOp = {}

        for (param, val) in request.GET.items():
            if param == 'page':
                queryOp['page'] = int(val)
                queryOp['page'] = queryOp['page'] if queryOp['page'] > 0 else 1
            elif param == 'order':
                _order = val
            elif param == 'records':
                queryOp['records'] = int(val)
            elif val:
                if len(val.split('.')) == 2:
                    [_op, _val] = val.split('.')
                    _filter.append(Q(**_OPERATORS[_op](param, _val)))
                else:
                    _filter.append(Q(**{param: val}))

        (items, paginator) = self.service.find(
            _filter, _order, **queryOp)
        _items = []

        if self.serializers:
            _items = self.serializers(items, many=True).data
        else:
            _items = [dump(item, True) for item in items]      
        reponse = {
            "data":	_items,
            "page": paginator.page,
            "records": paginator.records,
            "total": paginator.total,
        }
        return Response(reponse)

    def put(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        if isinstance(data, (list, tuple)):
            for item in data:
                self.__updateItem(item)
        elif isinstance(data, dict):
            self.__updateItem(data)
        else:
            return Response(status=400)
        return Response(status=204)

    def delete(self, request, id, *args, **kwargs):
        self.model.objects.get(pk=id).delete()
        return Response(status=204)

    def __updateItem(self, item):
        model = self.service.getById(item['id'])
        if model is None:
            return
        for key in item:
            setattr(model, key, item[key])
        model.save()
