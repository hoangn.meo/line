import pymongo
import json
import requests
from django.conf import settings
from . import Singleton


class MongoDB(Singleton):
    def __init__(self):
        dbclient = pymongo.MongoClient("mongodb://localhost:27017/")
        self.db = dbclient["bus"]
