from django.db import models

# Create your models here.


class TruckTypeModel(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class CargoTypeModel(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class DriverModel(models.Model):
    fullname = models.CharField(max_length=255)

    def __str__(self):
        return self.fullname


class TruckInfoModel(models.Model):
    plate = models.CharField(max_length=255)
    driver = models.ForeignKey(DriverModel, on_delete=models.CASCADE)
    truck_type = models.ForeignKey(
        TruckTypeModel, null=True, blank=True, on_delete=models.CASCADE)
    price = models.FloatField(default=0)
    dimension = models.CharField(max_length=255, null=True, blank=True)
    parking_address = models.CharField(max_length=500, null=True, blank=True)
    year = models.PositiveIntegerField(null=True, blank=True)
    status = models.PositiveIntegerField(default=0)
    description = models.TextField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.plate


class TruckCargoModel(models.Model):
    truck = models.ForeignKey(TruckInfoModel, on_delete=models.CASCADE)
    cargo = models.ForeignKey(CargoTypeModel, on_delete=models.CASCADE)
