from django.apps import AppConfig


class TruckInfoConfig(AppConfig):
    name = 'truckInfo'
