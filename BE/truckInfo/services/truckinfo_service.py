from core.modules.base_service import BaseService
from ..models import TruckInfoModel


class TruckInfoService(BaseService):
    def __init__(self):
        BaseService.__init__(self, TruckInfoModel, orderBy='id')
