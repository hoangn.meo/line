from core.modules.base_service import BaseService
from ..models import CargoTypeModel


class CargoTypeService(BaseService):
    def __init__(self):
        BaseService.__init__(self, CargoTypeModel, orderBy='id')
