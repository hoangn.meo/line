from core.modules.base_service import BaseService
from ..models import TruckTypeModel


class TruckTypeService(BaseService):
    def __init__(self):
        BaseService.__init__(self, TruckTypeModel, orderBy='id')
