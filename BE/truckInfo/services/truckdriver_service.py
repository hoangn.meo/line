from core.modules.base_service import BaseService
from ..models import DriverModel


class TruckDriverService(BaseService):
    def __init__(self):
        BaseService.__init__(self, DriverModel, orderBy='id')
