
from django.urls import path, include
from .views import TruckTypeAPI, CargoTypeAPI, TruckDriverAPI, TruckInfoAPI
urlpatterns = [
    path('truck-type/<int:id>/', TruckTypeAPI.as_view()),
    path('truck-type/', TruckTypeAPI.as_view()),

    path('cargo-type/<int:id>/', CargoTypeAPI.as_view()),
    path('cargo-type/', CargoTypeAPI.as_view()),

    path('truck-driver/<int:id>/', TruckDriverAPI.as_view()),
    path('truck-driver/', TruckDriverAPI.as_view()),

    path('truck-info/<int:id>/', TruckInfoAPI.as_view()),
    path('truck-info/', TruckInfoAPI.as_view()),

]
