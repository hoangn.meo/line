import re
import json
import os
from django.shortcuts import render
from rest_framework.views import APIView
from core.modules.base_api import BaseAPI
from rest_framework.response import Response
from django.http import Http404, HttpResponse
from .models import TruckTypeModel, CargoTypeModel, DriverModel, TruckInfoModel, TruckCargoModel
from .services.trucktype_service import TruckTypeService
from .services.cargotype_service import CargoTypeService
from .services.truckdriver_service import TruckDriverService
from .services.truckinfo_service import TruckInfoService
from django.db import transaction
from django.db.models import Q
from rest_framework import serializers

# Create your views here.


class TruckTypeAPI(BaseAPI):
    service = TruckTypeService()

    def __init__(self):
        BaseAPI.__init__(self, TruckTypeModel, self.service)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        obj = TruckTypeModel(
            name=data['name'],
        )
        obj.save()
        return Response(status=204)


class CargoTypeAPI(BaseAPI):
    service = CargoTypeService()

    def __init__(self):
        BaseAPI.__init__(self, CargoTypeModel, self.service)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        obj = CargoTypeModel(
            name=data['name'],
        )
        obj.save()
        return Response(status=204)


class TruckDriverAPI(BaseAPI):
    service = TruckDriverService()

    def __init__(self):
        BaseAPI.__init__(self, DriverModel, self.service)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        obj = DriverModel(
            fullname=data['fullname'],
        )
        obj.save()
        return Response(status=204)


class TruckCargoSerializers(serializers.ModelSerializer):
    class Meta:
        model = TruckCargoModel
        fields = ['cargo']
        depth = 1


class TruckSerializers(serializers.ModelSerializer):
    cargo_types = TruckCargoSerializers(
        many=True, source='truckcargomodel_set')

    class Meta:
        model = TruckInfoModel
        fields = '__all__'
        depth = 1


class TruckInfoAPI(BaseAPI):
    service = TruckInfoService()
    truck_serializers = TruckSerializers

    def __init__(self):
        BaseAPI.__init__(self, TruckInfoModel, self.service,
                         self.truck_serializers)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        with transaction.atomic():
            obj = TruckInfoModel(
                plate=data['plate'],
                truck_type_id=data['truck_type_id'],
                driver_id=data['driver_id'],
                price=data['price'],
                dimension=data.get("dimension", None),
                year=data.get("year", None),
                parking_address=data.get("parking_address", None),
                status=data['status'],
                description=data.get("description", None),
            )
            obj.save()
            cargo_types = data.get("cargo_types", [])
            for t in cargo_types:
                TruckCargoModel(
                    cargo_id=t,
                    truck_id=obj.id
                ).save()
        return Response(status=204)

    def put(self, request, id, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        truck_info = self.service.getById(id)
        if truck_info is None:
            return HttpResponse(status=404)
        with transaction.atomic():
            truck_info.plate = data['plate']
            truck_info.truck_type_id = data['truck_type_id']
            truck_info.driver_id = data['driver_id']
            truck_info.price = data['price']
            truck_info.dimension = data.get("dimension", None)
            truck_info.year = data.get("year", None)
            truck_info.parking_address = data.get("parking_address", None)
            truck_info.status = data['status']
            truck_info.description = data.get("description", None)
            TruckCargoModel.objects.filter(truck_id=id).all().delete()
            cargo_types = data.get("cargo_types", [])
            for t in cargo_types:
                TruckCargoModel(
                    cargo_id=t,
                    truck_id=id
                ).save()
            truck_info.save()
        return HttpResponse(status=204)
